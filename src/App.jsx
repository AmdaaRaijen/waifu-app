import { useEffect, useState } from "react";
import BarLoader from "react-spinners/BarLoader";

function App() {
  const [gambar, setGambar] = useState("");
  const [category, setcategory] = useState("waifu");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    handleFetch();
  }, []);

  const handleFetch = async () => {
    setLoading(true);
    const res = await fetch(`https://api.waifu.pics/sfw/${category}`);
    if (res.ok) {
      const data = await res.json();
      setGambar(data.url);
    }
    setLoading(false);
  };

  const handlecategory = (e) => {
    setcategory(e.target.value);
  };

  return (
    <section className="h-screen flex flex-col justify-center items-center gap-5">
      <h1 className="font-medium text-xl">Waifu App</h1>

      <select
        onChange={handlecategory}
        value={category}
        className="select select-bordered w-full max-w-xs"
      >
        <option value="waifu">Waifu</option>
        <option value="neko">Neko</option>
        <option value="shinobu">Shinobu</option>
        <option value="megumin">Megumin</option>
        <option value="bully">Bully</option>
        <option value="cry">Cry</option>
        <option value="hug">Hug</option>
        <option value="awoo">Awoo</option>
        <option value="pat">Pat</option>
        <option value="smug">Smug</option>
        <option value="blush">Blush</option>
        <option value="smile">Smile</option>
        <option value="wave">Wave</option>
        <option value="dance">Dance</option>
      </select>

      <button className="btn gap-2" onClick={handleFetch}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
          />
        </svg>
        Waifu
      </button>

      <div className="card w-96 bg-base-100 shadow-xl">
        <figure className="px-10 pt-10">
          {loading ? (
            <div className="columns-1 text-center">
              <BarLoader />
              <p>loading ...</p>
            </div>
          ) : (
            <img
              src={gambar}
              alt="Waifu"
              className="rounded-xl"
              loading="lazy"
            />
          )}
        </figure>
        <div className="card-body items-center text-center">
          <h2 className="card-title">Your Waifu</h2>
        </div>
      </div>
    </section>
  );
}

export default App;
